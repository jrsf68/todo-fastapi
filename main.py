from fastapi import FastAPI, status, HTTPException
from typing import List

from database import create_item, delete_item, read_item, read_list, update_item
import schemas


app = FastAPI()


@app.get('/')
def root():
    return 'todooo'


@app.post('/todo', status_code=status.HTTP_201_CREATED)
def create_todo(todo: schemas.ToDo):
    tododb = create_item(todo)
    if not tododb:
        raise HTTPException(status_code=404, detail=f'todo item without description!')
    return tododb


@app.get('/todo{id}', response_model=schemas.ToDo)
def read_todo(id: int):
    tododb = read_item(id)
    if not tododb:
        raise HTTPException(status_code=404, detail=f"todo item with id {id} not found.")
    return tododb


@app.get('/todo/', response_model=List[schemas.ToDo])
def read_todo_list():
    todo_list = read_list()
    if not todo_list:
        raise HTTPException(status_code=404, detail="todo list is empty!")
    return todo_list
    

@app.put('/todo/{id}')
def update_todo(id: int, task: str):
    tododb = update_item(id, task)
    if not tododb:
        raise HTTPException(status_code=404, detail=f'todo item with id {id} not found.')
    return tododb


@app.delete('/todo{id}')
def delete_todo(id: int):
    tododb = delete_item(id)
    if not tododb:
        raise HTTPException(status_code=404, detail=f"todo item with id {id} not found.")
    return tododb
