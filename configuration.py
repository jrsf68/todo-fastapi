# dialect+driver://username:password@host:port/database
# engine = create_engine('postgresql+psycopg2://scott:tiger@localhost/mydatabase')
# engine = create_engine('mysql://scott:tiger@localhost/foo')
# engine = create_engine('sqlite:///todooo.db')

# DB_DIALECT = 'sqlite'
# DB_DRIVER = '://'
# DB_USER = ''
# DB_PASSWORD = ''
# DB_HOST = ''
# DB_PORT = ''
# DB_DATABASE = '/todooo.db'

DB_DIALECT = 'postgresql'
DB_DRIVER = '://'
DB_USER = 'todo:'
DB_PASSWORD = 'todo'
DB_HOST = '@0.0.0.0'
DB_PORT = ':5432'
DB_DATABASE = '/todo'

DB_URL = DB_DIALECT + DB_DRIVER + DB_USER + DB_PASSWORD + DB_HOST + DB_PORT + DB_DATABASE

