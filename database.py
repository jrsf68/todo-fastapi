from sqlalchemy import create_engine 
from sqlalchemy.orm import Session

from configuration import DB_URL
import models
import schemas


engine = create_engine(DB_URL)
models.Base.metadata.create_all(engine)


def create_item(todo: schemas.ToDoCreate):
    session = Session(bind=engine, expire_on_commit=False)
    if todo.task is None or todo.task == 'string':
        return None
    tododb = models.ToDo(task=todo.task)
    session.add(tododb)
    session.commit()
    session.refresh(tododb)
    session.close()
    return tododb


def read_item(id: int):
    session = Session(bind=engine, expire_on_commit=False)
    tododb = session.query(models.ToDo).get(id)
    session.close()
    return tododb


def read_list():
    session = Session(bind=engine, expire_on_commit=False)
    tododb = session.query(models.ToDo).all()
    session.close()
    return tododb


def update_item(id: int, task: str):
    session = Session(bind=engine, expire_on_commit=False)
    tododb = session.query(models.ToDo).get(id)
    session.close()
    return tododb


def update_item(id: int, task: str):
    session = Session(bind=engine, expire_on_commit=False)
    tododb = session.query(models.ToDo).get(id)
    if not tododb:
        return None
    tododb.task=task
    session.commit()
    session.close()
    return tododb


def delete_item(id: int):
    session = Session(bind=engine, expire_on_commit=False)
    tododb = session.query(models.ToDo).get(id)
    if not tododb:
        return None
    session.delete(tododb)
    session.commit()
    session.close()
    return tododb
